from .wallet import Wallet

async def setup(bot):
    cog = Wallet(bot)
    bot.add_cog(cog)

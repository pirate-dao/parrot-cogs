import aiohttp
import binascii
import base58
import discord
import json
import logging

from redbot.core import Config, commands
from typing import Optional

cog_module = "Parrot-Cogs.Wallet"
log = logging.getLogger(f"red.{cog_module.lower()}")

UNIQUE_ID = f"0x{binascii.hexlify(cog_module.encode()).decode()}"
HELIUM_ACCOUNTS_API = "https://api.helium.io/v1/accounts/"


class Wallet(commands.Cog):
    """Provides Helium wallet information"""
    def __init__(self, bot):
        self.bot = bot
        self.conf = Config.get_conf(self, identifier=UNIQUE_ID, force_registration=True)
        self.conf.register_user(wallet_id="")

    @staticmethod
    async def check_wallet_id(wallet_id: str, ctx: commands.Context) -> bool:
        try:
            base58.b58decode_check(wallet_id)
            return True
        except Exception as e:
            await ctx.send(f"Invalid wallet ID: {e}")
        return False

    @staticmethod
    async def get_wallet_balance(wallet_id, ctx: commands.Context):
        async with aiohttp.ClientSession() as session:
            try:
                async with session.get(HELIUM_ACCOUNTS_API + wallet_id) as response:
                    info = await response.read()
                    return json.loads(info).get("data")
            except aiohttp.client_exceptions.ClientConnectionError as e:
                log.exception(f"Wallet balance fetch failed for id {wallet_id}", exc_info=e)
                await ctx.send(f"⚠️ Error getting wallet balance: {e}")

    @commands.group()
    async def wallet(self, ctx: commands.Context):
        """Get Helium wallet information"""
        pass

    @wallet.command()
    async def associate(self, ctx: commands.Context, wallet_id: Optional[str] = None):
        """Associate a Helium wallet with your profile in this guild"""
        if wallet_id and not await self.check_wallet_id(wallet_id, ctx):
            return
        await self.conf.user(ctx.author).wallet_id.set(wallet_id)
        await ctx.send(f"Wallet ID {'re' if not wallet_id else ''}set")

    @wallet.command()
    async def balance(self, ctx: commands.Context, wallet_id: Optional[str] = None):
        """Get the current balance of a Helium wallet"""
        if wallet_id:
            if not await self.check_wallet_id(wallet_id, ctx):
                return
        else:
            wallet_id = await self.conf.user(ctx.author).wallet_id()
        if not wallet_id:
            await ctx.send("⚠️ No wallet ID provided or associated with you")
            return
        wallet_info = await self.get_wallet_balance(wallet_id, ctx)
        if not wallet_info:
            await ctx.send(f"⚠️ No balance found for {wallet_id}")
            return
        balance = wallet_info.get("balance")
        await ctx.send(f" The current balance is: {balance} HNT")

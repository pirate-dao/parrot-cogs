from .speak import Speak

async def setup(bot):
    cog = Speak(bot)
    bot.add_cog(cog)

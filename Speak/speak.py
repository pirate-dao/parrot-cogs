import aiohttp
import binascii
import discord
import logging
import random
import re

from redbot.core import checks, Config, commands
from typing import Optional

cog_module = "Parrot-Cogs.Speak"
log = logging.getLogger(f"red.{cog_module.lower()}")

UNIQUE_ID = f"0x{binascii.hexlify(cog_module.encode()).decode()}"
TOKENIZER = re.compile(r'(.{3})')
CONTROL = UNIQUE_ID


class Speak(commands.Cog):
    """ Another markov-chain-based text generator cog """
    def __init__(self, bot):
        self.bot = bot
        self.conf = Config.get_conf(self, identifier=UNIQUE_ID, force_registration=True)
        self.conf.register_guild(model={})

    @commands.command()
    async def train(self, ctx: commands.Context, *, text: str):
        """ Train the bot on some input text """
        # Load the guild's markov chain
        try:
            model = await self.conf.guild(ctx.guild).model()
        except AttributeError:
            await ctx.send("This command is not available in direct messages")
            return
        # Begin all state chains with the control marker
        state = CONTROL
        # Remove code block formatting and outer whitespace
        content = text.replace('`', '').strip()
        # Split message into tokens
        tokens = TOKENIZER.split(content)
        # Add control character transition to end of token chain
        tokens.append(state)
        # Iterate over the tokens in the message
        for i, token in enumerate(tokens):
            # Ensure dict key for vector distribution is created
            model[state] = model.get(state, {})
            # Increment the weight for this state vector or initialize it to 1
            model[state][token] = model[state].get(token, 0) + 1
            # Produce bounded sliding state window
            j = 1 + i - 2 if i >= 2 else 0
            state = "".join(tokens[j:i+1])
        # Store the updated model
        await self.conf.guild(ctx.guild).model.set(model)
        await ctx.send(await self.generate_text(model, tokens[0]))

    @commands.command()
    async def speak(self, ctx: commands.Context, initial_state: Optional[str] = None):
        """ Generate text, optionally specifying a word to start with """
        try:
            model = await self.conf.guild(ctx.guild).model()
        except AttributeError:
            await ctx.send("This command is not available in direct messages")
            return
        text = await self.generate_text(model, initial_state)
        await ctx.send(text[:2000])

    @staticmethod
    async def generate_text(model: dict, initial_state: Optional[str] = None):
        """ Generate text based on trained model """
        output = []
        i = 0
        gram = ""
        # Begin in a state of transitioning from message boundary if not specified
        state = CONTROL
        # If initial state is specified, migrate to it
        if initial_state:
            model_state = model[state].get(initial_state)
            state = initial_state
            if not model_state:
                return "⚠️ Error: Initial state not found"
        else:
            model_state = model
        while gram.strip() != CONTROL:
            model_state = model_state[state[-1]]
            # Generate and store next gram
            gram, = random.choices(
                population=list(model_state.keys()),
                weights=list(model_state.values()),
                k=1
            )
            output.append(gram)
            # Produce bounded sliding state window
            i += 1
            j = i - 2 if i > 2 else 0
            state = "".join(output[j:i])
        if not output:
            return "⚠️ Error: Generated nothing"
        return "".join(output[:-1])

    @checks.admin_or_permissions(manage_guild=True)
    @commands.command(hidden=True)
    async def reset_speak(self, ctx: commands.Context):
        await self.conf.guild(ctx.guild).model.set({})
        await ctx.send("Trained speech model has been deleted")
